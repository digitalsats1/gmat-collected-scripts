% -*- mode: gmat; coding: utf-8 -*-
%Time-stamp: "2021-07-23 17:39:31 kastner"

%--------------------------
%---------- Script Info
%--------------------------

%General Mission Analysis Tool(GMAT) Script
%Original:
%  /home/vagrant/gmat/application/samples/Tut_SimulatingAnOrbit.script
%  -or-
%  /home/vagrant/gmat/GMAT-R2020a-Linux-x64/samples/Tut_SimulatingAnOrbit.script
%Created: 2011-11-23 10:08:38
%Tutorial:
%  http://gmat.sourceforge.net/docs/R2020a/html/SimulatingAnOrbin.html

%Saved to: /home/vagrant/gmat/GMAT-R2020a-Linux-x64/bin/

%--------------------------
%---------- Setup
%--------------------------

% Depending on how you start GMATd, your root will be
%    /home/vagrant/gmat
%    /home/vagrant/gmat/GMAT-R2020a-Linux-x64
% (the second one is probably the one your are using).
%
% So your gmat_startup_file.txt may be in different places:
%
%    Edit /home/vagrant/gmat/application/bin/gmat_startup_file.txt
% -or-
%    Edit /home/vagrant/gmat/GMAT-R2020a-Linux-x64/bin/gmat_startup_file.txt
%
%     Comment out MATLAB related lines (comment lines start with #):
%     #PLUGIN   = ../plugins/libMatlabInterface
%     #PLUGIN   = ../plugins/libFminconOptimizer
%     ...
%     #MATLAB_FUNCTION_PATH   = ROOT_PATH/matlab
%     #MATLAB_FUNCTION_PATH   = ROOT_PATH/userfunctions/matlab
%
% To avoid future confusion, you may also find that you need to
% explicitly change your root path
%
%     ROOT_PATH     = /home/vagrant/gmat/GMAT-R2020a-Linux-x64/
%
% Finally, you can comment out unused items that will cause errors messages
% during loading
%
%     # Internal-only-plugins
%     #PLUGIN   = ../plugins/proprietary/libCsaltInterfaced
%     #PLUGIN   = ../plugins/proprietary/libEMTGmodelsd
%     #PLUGIN   = ../plugins/proprietary/libMarsGRAMd
%     #PLUGIN   = ../plugins/proprietary/libMsise86d
%     #PLUGIN   = ../plugins/proprietary/libNRLMsise00d
%     #PLUGIN   = ../plugins/proprietary/libSNOptimizerd
%     #PLUGIN   = ../plugins/proprietary/libVF13Optimizerd
%
% Note: In the future, we may be able to substitute GNU's open source
%       replacement, Octave, for proprietary product MATLAB.
%
% Copy atmosphere drag force model into expected directory
%
%    cd /home/vagrant/gmat/application
%    cp samples/SupportFiles/CSSI_2004To2026.txt data/atmosphere/earth/
% -or-
%    cd /home/vagrant/gmat/GMAT-R2020a-Linux-x64
%    cp samples/SupportFiles/CSSI_2004To2026.txt data/atmosphere/earth/

%----------------------------------------
%---------- Spacecraft
%----------------------------------------

Create Spacecraft DigitalSat;

%--------------------------
%---------- Simulation Time
%--------------------------

GMAT DigitalSat.DateFormat = UTCGregorian;
% GMAT DigitalSat.Epoch = '22 Jul 2014 11:29:10.811';
GMAT DigitalSat.Epoch = '24 Jan 2024 12:00:00.000';

%--------------------------
%---------- Coordinates
%--------------------------

GMAT DigitalSat.CoordinateSystem = EarthMJ2000Eq;

%--------------------------
%---------- Orbit
%--------------------------

% Assume launch from International Space Station
%    perigee altitude: 418 km (259.7 mi) AMSL [b]
%    apogee altitude:  422 km (262.2 mi) AMSL [b]
%    orbital inclination: 52.64 degrees [b]
%    orbital speed: 7.66 km/s (27,600 km/h or 17,100 mph) [b]
%    orbital period: 92.68 minutes [b]
%    orbits per day: 15.49 [b]
%       [b] https://en.wikipedia.org/wiki/International_Space_Station

% Orbital parameters.
%   Keplerian assumption that mass of spacecraft is too small
%   to significatly affect the orbit of the Earth.
GMAT DigitalSat.DisplayStateType = Keplerian;
% semi-major axis (kilometers)
GMAT DigitalSat.SMA = 83474.31800000001;
% eccentricity (unitless fraction)
GMAT DigitalSat.ECC = 0.89652;
% inclination (degrees)
GMAT DigitalSat.INC = 12.4606;
% right ascension of the ascending node
GMAT DigitalSat.RAAN = 292.8362;
% argument of periapsis (degrees)
GMAT DigitalSat.AOP = 218.9805;
% true anomaly (degrees)
GMAT DigitalSat.TA = 180;

%--------------------------
%---------- Physical
%--------------------------

% CubeSat
%    classification: Nanosatellite
%    cube is 10cm x 10cm x 10cm [a]
%       10cm = 0.1m
%    mass is 1.33 kilograms (2.9 lb) per unit [a]
%       [a] https://en.wikipedia.org/wiki/CubeSat

% mass (kilograms)
%GMAT DigitalSat.DryMass = 850;
GMAT DigitalSat.DryMass = 1.33;

% coefficient of drag (unitless)
%    Between heights of 140 and 400 km at times of low solar activity,
%    or heights of 140 and 600 km at times of high solar activity,
%    the drag coefficient is almost independent of height, and because
%    of the lack of decisive evidence, for now, there is perhaps not
%    sufficient reason to abondon the default value of 2.2 which has
%    been widely used for years. Note that this value can have an
%    uncertainty between 10 and 15 percent.
%    [c] https://www.sciencedirect.com/science/article/abs/pii/0032063365901509
GMAT DigitalSat.Cd = 2.2;

% coefficient of reflectivity (unitless)
GMAT DigitalSat.Cr = 1.8;

% drag area (square meters)
%GMAT DigitalSat.DragArea = 15;
GMAT DigitalSat.DragArea = 0.1;
% solar radiation pressure area (square meters)
GMAT DigitalSat.SRPArea = 1;

%--------------------------
%---------- Identification
%--------------------------
% SPICE - NASA's Spacecraft Planet Instrument C-matrix Events information
% NAIF  - NASA's Navigation and Ancillary Information Facility identification

% If truly using SPACE and NAIF, then SPK, FK, CK and/or SCLK files should
% also be included.

% NAIF identification (just using a placeholder for now)
GMAT DigitalSat.NAIFId = -123456789;
% NAIF reference frame (just using a placeholder for now)
GMAT DigitalSat.NAIFIdReferenceFrame = -123456789;

% Just a moniker for reference
% GMAT DigitalSat.Id = 'DigitalSatId';
GMAT DigitalSat.Id = 'SmallSatsOne';

%--------------------------
%---------- 6DoF
%--------------------------

% Attitude model is one of: CoordinateSystemFixed, Spinner,
%    PrecessingSpinner, NadirPointing, CCSDS-AEM, SpiceAttitude,
%    ThreeAxisKinematic
GMAT DigitalSat.Attitude = CoordinateSystemFixed;
GMAT DigitalSat.ModelFile = '../data/vehicle/models/aura.3ds';

% Starting position (translation: X: surge, Y: heave, Z: sway)
GMAT DigitalSat.ModelOffsetX = 0;
GMAT DigitalSat.ModelOffsetY = 0;
GMAT DigitalSat.ModelOffsetZ = 0;

% Starting position (rotation: X: pitch, Y: roll, Z: yaw)
GMAT DigitalSat.ModelRotationX = 0;
GMAT DigitalSat.ModelRotationY = 0;
GMAT DigitalSat.ModelRotationZ = 0;

% Scaling of the model
GMAT DigitalSat.ModelScale = 1;

% Spatial rotation
GMAT DigitalSat.AttitudeDisplayStateType = 'Quaternion';
GMAT DigitalSat.AttitudeRateDisplayStateType = 'AngularVelocity';
GMAT DigitalSat.AttitudeCoordinateSystem = 'EarthMJ2000Eq';

%----------------------------------------
%---------- Forces
%----------------------------------------

Create ForceModel LowEarthProp_ForceModel;

% Low altitude Earth modelling
GMAT LowEarthProp_ForceModel.CentralBody = Earth;
GMAT LowEarthProp_ForceModel.PrimaryBodies = {Earth};
GMAT LowEarthProp_ForceModel.PointMasses = {Luna, Sun};

% Solar radiation pressure modelling
GMAT LowEarthProp_ForceModel.SRP = On;

% Correction for relativistic speeds
GMAT LowEarthProp_ForceModel.RelativisticCorrection = Off;

% Error control is one of: None, RSSStep, RSSState, LargestStep, LargestState
GMAT LowEarthProp_ForceModel.ErrorControl = RSSStep;
GMAT LowEarthProp_ForceModel.GravityField.Earth.Degree = 10;
GMAT LowEarthProp_ForceModel.GravityField.Earth.Order = 10;
GMAT LowEarthProp_ForceModel.GravityField.Earth.PotentialFile = 'JGM2.cof';
GMAT LowEarthProp_ForceModel.GravityField.Earth.TideModel = 'None';

% Atmospheric drag modelling
GMAT LowEarthProp_ForceModel.Drag.AtmosphereModel = 'JacchiaRoberts';
GMAT LowEarthProp_ForceModel.Drag.F107 = 150;
GMAT LowEarthProp_ForceModel.Drag.F107A = 150;
GMAT LowEarthProp_ForceModel.Drag.MagneticIndex = 3;
GMAT LowEarthProp_ForceModel.Drag.F107 = 150;
GMAT LowEarthProp_ForceModel.Drag.F107A = 150;
GMAT LowEarthProp_ForceModel.Drag.MagneticIndex = 3;

% Solar radiation pressure modelling
GMAT LowEarthProp_ForceModel.SRP.Flux = 1367;
GMAT LowEarthProp_ForceModel.SRP.Nominal_Sun = 149597870.691;

%----------------------------------------
%---------- Propagators
%----------------------------------------

Create Propagator LowEarthProp;

% Propagate the effects of the central body: Earth
GMAT LowEarthProp.FM = LowEarthProp_ForceModel;

% Using a Runge Kutta 89 algorithm to integrate curves
% with the following parameters.
GMAT LowEarthProp.Type = RungeKutta89;
GMAT LowEarthProp.InitialStepSize = 60;
GMAT LowEarthProp.Accuracy = 9.999999999999999e-012;
GMAT LowEarthProp.MinStep = 0.001;
GMAT LowEarthProp.MaxStep = 2700;
GMAT LowEarthProp.MaxStepAttempts = 50;

% Monitor cumulative errors in integration calculations
GMAT LowEarthProp.StopIfAccuracyIsViolated = true;

%----------------------------------------
%---------- Burns
%----------------------------------------

% Rather than simulating just gravity, add a thrust.
% Here, we're modelling an instantaneous delta-V rather than a
% finite burn which is not instanatneous).
Create ImpulsiveBurn DefaultIB;

% Coordinate system
GMAT DefaultIB.CoordinateSystem = Local;
GMAT DefaultIB.Origin = Earth;
GMAT DefaultIB.Axes = VNB;

% Delta-V vector (km/s)
GMAT DefaultIB.Element1 = 0;
GMAT DefaultIB.Element2 = 0;
GMAT DefaultIB.Element3 = 0;

% Do not account for the loss of mass due to the burn (assume negligible)
GMAT DefaultIB.DecrementMass = false;

% Impulsive burn's specific impulse (unitless)
% ("specific" just means "divided by weight")
% Mathematically Isp is a ratio of the thrust produced to the weight
% flow of the propellants.
%    [e] https://www.grc.nasa.gov/WWW/k-12/airplane/specimp.html
GMAT DefaultIB.Isp = 300;

% Force of Earth's gravity (meters per second per second = m/sec^2)
% (9.81 m/sec^2 = 32.2 ft/sec^2)
GMAT DefaultIB.GravitationalAccel = 9.810000000000001;

%----------------------------------------
%---------- Subscribers
%----------------------------------------
% Decide which graphical elements will require modelling

Create OrbitView DefaultOrbitView;

% Only show the orbital visualization relative to Earth
GMAT DefaultOrbitView.SolverIterations = Current;
GMAT DefaultOrbitView.UpperLeft = [ 0 0 ];
GMAT DefaultOrbitView.Size = [ 0 0 ];
GMAT DefaultOrbitView.RelativeZOrder = 0;
GMAT DefaultOrbitView.Add = {DigitalSat, Earth};
GMAT DefaultOrbitView.CoordinateSystem = EarthMJ2000Eq;
GMAT DefaultOrbitView.DrawObject = [ true true ];
GMAT DefaultOrbitView.DataCollectFrequency = 1;
GMAT DefaultOrbitView.UpdatePlotFrequency = 50;
GMAT DefaultOrbitView.NumPointsToRedraw = 0;
GMAT DefaultOrbitView.ShowPlot = true;
GMAT DefaultOrbitView.ViewPointReference = Earth;
GMAT DefaultOrbitView.ViewPointVector = [ -60000 30000 20000 ];
GMAT DefaultOrbitView.ViewDirection = Earth;
GMAT DefaultOrbitView.ViewScaleFactor = 1;
GMAT DefaultOrbitView.ViewUpCoordinateSystem = EarthMJ2000Eq;

% Show Z axis as up (Earth's north pole)
GMAT DefaultOrbitView.ViewUpAxis = Z;

% Do not show the eclipted pane (major planetary orbital plane)
GMAT DefaultOrbitView.EclipticPlane = Off;
GMAT DefaultOrbitView.XYPlane = Off;
GMAT DefaultOrbitView.WireFrame = Off;
GMAT DefaultOrbitView.Axes = On;
GMAT DefaultOrbitView.Grid = Off;
GMAT DefaultOrbitView.SunLine = Off;
GMAT DefaultOrbitView.UseInitialView = On;

% Show major stars in the background
GMAT DefaultOrbitView.StarCount = 7000;
GMAT DefaultOrbitView.EnableStars = On;
GMAT DefaultOrbitView.EnableConstellations = On;


%----------------------------------------
%---------- Mission Sequence
%----------------------------------------

% Run the simulation to periapsis (point at which the satelite is
% closest to the center of mass of the Earth, a.k.a. closest approach)
BeginMissionSequence;
Propagate LowEarthProp(DigitalSat) {DigitalSat.Earth.Periapsis};
