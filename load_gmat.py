# GMAT Application Programmer's Interface Example
#
# Coded by D. Conway. Thinking Systems, Inc.
#
# This file is a template for files used run the GMAT API from a folder outside
# of the GMAT application folder.

import sys
from os import path

apistartup = "api_startup_file.txt"
#GmatInstall = "<TopLevelGMATFolder>"
GmatInstall = "/home/vagrant/gmat/GMAT-R2020a-Linux-x64"
GmatBinPath = GmatInstall + "/bin"
Startup = GmatBinPath + "/" + apistartup

if path.exists(Startup):

   sys.path.insert(1, GmatBinPath)

   # Added application
   Application = "/home/vagrant/gmat/application"
   ApplicationBinPath = Application + "/bin"
   ApplicationGMATPy = ApplicationBinPath + "/gmatpy"

   # Added pathing to application
   sys.path.insert(1, ApplicationBinPath)

   # Added station
   PlugIns = "/home/vagrant/gmat/plugins"
   StationPluginPath = PlugIns + "/StationPlugin" + "/swig"

   # Added pathing to station
   sys.path.insert(1, StationPluginPath)

   # Added navigation
   PlugIns = "/home/vagrant/gmat/plugins"
   NavigationPluginPath = PlugIns + "/EstimationPlugin" + "/swig"

   # Added pathing to navigation
   sys.path.insert(1, NavigationPluginPath)

   import gmatpy as gmat
   gmat.Setup(Startup)

else:
   print("Cannot find ", Startup)
   print()
   print("Please set up a GMAT startup file named ", apistartup, " in the ",
      GmatBinPath, " folder.")
